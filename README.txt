DESCRIPTION
------------------------------
The Organic Groups E-mail Blast module enables group admins to send e-mail
blasts to group members.

FEATURES
------------------------------
Group members’ e-mail addresses are sent in the Bcc field.
The site’s default e-mail address is used as the sender in order to minimize
spam detection.
The group admin’s e-mail address is used for the Reply-To field.
E-mail blasts are logged to the watchdog table.

REQUIREMENTS
------------------------------
Organic groups 7.x-1.x
